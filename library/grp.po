# Copyright (C) 2001-2018, Python Software Foundation
# For licence information, see README file.
#
msgid ""
msgstr ""
"Project-Id-Version: Python 3\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-01-15 22:33+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: FRENCH <traductions@lists.afpy.org>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: library/grp.rst:2
msgid ":mod:`grp` --- The group database"
msgstr ""

#: library/grp.rst:10
msgid ""
"This module provides access to the Unix group database. It is available on "
"all Unix versions."
msgstr ""

#: includes/wasm-notavail.rst:None
msgid ":ref:`Availability <availability>`: not Emscripten, not WASI."
msgstr ""

#: includes/wasm-notavail.rst:5
msgid ""
"This module does not work or is not available on WebAssembly platforms "
"``wasm32-emscripten`` and ``wasm32-wasi``. See :ref:`wasm-availability` for "
"more information."
msgstr ""

#: library/grp.rst:15
msgid ""
"Group database entries are reported as a tuple-like object, whose attributes "
"correspond to the members of the ``group`` structure (Attribute field below, "
"see ``<grp.h>``):"
msgstr ""

#: library/grp.rst:20
msgid "Index"
msgstr "Index"

#: library/grp.rst:20
msgid "Attribute"
msgstr "Attribut"

#: library/grp.rst:20
msgid "Meaning"
msgstr "Signification"

#: library/grp.rst:22
msgid "0"
msgstr "0"

#: library/grp.rst:22
msgid "gr_name"
msgstr "gr_name"

#: library/grp.rst:22
msgid "the name of the group"
msgstr ""

#: library/grp.rst:24
msgid "1"
msgstr "1"

#: library/grp.rst:24
msgid "gr_passwd"
msgstr "gr_passwd"

#: library/grp.rst:24
msgid "the (encrypted) group password; often empty"
msgstr ""

#: library/grp.rst:27
msgid "2"
msgstr "2"

#: library/grp.rst:27
msgid "gr_gid"
msgstr "gr_gid"

#: library/grp.rst:27
msgid "the numerical group ID"
msgstr ""

#: library/grp.rst:29
msgid "3"
msgstr "3"

#: library/grp.rst:29
msgid "gr_mem"
msgstr "gr_mem"

#: library/grp.rst:29
msgid "all the group member's  user names"
msgstr ""

#: library/grp.rst:33
msgid ""
"The gid is an integer, name and password are strings, and the member list is "
"a list of strings. (Note that most users are not explicitly listed as "
"members of the group they are in according to the password database.  Check "
"both databases to get complete membership information.  Also note that a "
"``gr_name`` that starts with a ``+`` or ``-`` is likely to be a YP/NIS "
"reference and may not be accessible via :func:`getgrnam` or :func:"
"`getgrgid`.)"
msgstr ""

#: library/grp.rst:40
msgid "It defines the following items:"
msgstr ""

#: library/grp.rst:45
msgid ""
"Return the group database entry for the given numeric group ID. :exc:"
"`KeyError` is raised if the entry asked for cannot be found."
msgstr ""

#: library/grp.rst:48
msgid ""
":exc:`TypeError` is raised for non-integer arguments like floats or strings."
msgstr ""

#: library/grp.rst:53
msgid ""
"Return the group database entry for the given group name. :exc:`KeyError` is "
"raised if the entry asked for cannot be found."
msgstr ""

#: library/grp.rst:59
msgid "Return a list of all available group entries, in arbitrary order."
msgstr ""

#: library/grp.rst:65
msgid "Module :mod:`pwd`"
msgstr "Module :mod:`pwd`"

#: library/grp.rst:65
#, fuzzy
msgid "An interface to the user database, similar to this."
msgstr "Interface pour la base de données des groupes, similaire à celle-ci."

#: library/grp.rst:67
#, fuzzy
msgid "Module :mod:`spwd`"
msgstr "Module :mod:`pwd`"

#: library/grp.rst:68
#, fuzzy
msgid "An interface to the shadow password database, similar to this."
msgstr ""
"Interface pour la base de données (normale) des mots de passe, semblable à "
"ceci."
